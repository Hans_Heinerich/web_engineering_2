#FROM openjdk:8-jdk-alpine
#ARG JAR_FILE=.mvn/wrapper/*.jar
#COPY ${JAR_FILE} maven-wrapper.jar
#ENTRYPOINT ["java","-jar","/maven-wrapper.jar"]

FROM maven:3.8-openjdk-15 as builder
COPY ./ ./
RUN mvn package -DskipTests=true

FROM openjdk:15-jdk-alpine
RUN mkdir -p /target
COPY --from=builder target/demo-0.0.1-SNAPSHOT.jar /target/demo-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/target/demo-0.0.1-SNAPSHOT.jar"]

