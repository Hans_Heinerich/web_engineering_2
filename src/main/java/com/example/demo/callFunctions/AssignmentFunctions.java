package com.example.demo.callFunctions;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.DataSave.AssignmentDataStorage;
import com.example.demo.DataSave.EmployeeDataStorage;
import com.example.demo.DataStructures.AssignmentDataSt;
import com.example.demo.callFunctionsInterfaces.AssignmentFunctionsInterface;

@Service
public class AssignmentFunctions implements AssignmentFunctionsInterface {

    @Autowired
    AssignmentDataStorage assignmentDatabase;

    @Autowired
    EmployeeDataStorage employeeDatabase;

    @Override
    public List<AssignmentDataSt> getAllAssignments() {
        List<AssignmentDataSt> retList = assignmentDatabase.findAll();
        if (retList.isEmpty()) {
            return retList;
        }
        throw new NoSuchElementException();
    }

    @Override
    public AssignmentDataSt addAssignment(UUID employeeUuid, UUID reservationUuid, String role) {
        employeeDatabase.findById(employeeUuid).orElseThrow();
        // ToDo check if reservation exist by calling other service.
        return assignmentDatabase.save(new AssignmentDataSt(employeeUuid, reservationUuid, role));
    }

    @Override
    public AssignmentDataSt getAssignment(UUID id) {
        return assignmentDatabase.findById(id).orElseThrow();
    }

    @Override
    public AssignmentDataSt setAssignment(UUID id, UUID employeeUuid, UUID reservationUuid, String role) {
        employeeDatabase.findById(employeeUuid).orElseThrow();
        // ToDo Check if reservation exists
        AssignmentDataSt assignment = assignmentDatabase.findById(id).orElseThrow();
        assignment.setEmployee_id(employeeUuid);
        assignment.setReservation_id(reservationUuid);
        assignment.setRole(role);
        return assignmentDatabase.save(assignment);
    }

    @Override
    public boolean removeAssignment(UUID id) {
        getAssignment(id);
        assignmentDatabase.deleteById(id);
        return true;
    }

}
