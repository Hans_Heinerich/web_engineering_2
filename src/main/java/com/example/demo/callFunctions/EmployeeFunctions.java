package com.example.demo.callFunctions;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.DataSave.AssignmentDataStorage;
import com.example.demo.DataSave.EmployeeDataStorage;
import com.example.demo.DataStructures.AssignmentDataSt;
import com.example.demo.DataStructures.EmployeeDataSt;
import com.example.demo.callFunctionsInterfaces.EmployeeFunctionsInterface;

@Service
public class EmployeeFunctions implements EmployeeFunctionsInterface {

    @Autowired
    EmployeeDataStorage employeeDatabase;

    @Autowired
    AssignmentDataStorage assignmentDatabase;

    @Override
    public List<EmployeeDataSt> getAllEmployees() {
        List<EmployeeDataSt> retList = employeeDatabase.findAll();
        if (retList.isEmpty()) {
            return retList;
        }
        throw new NoSuchElementException();
    }

    @Override
    public EmployeeDataSt addEmployee(String name) {
        return employeeDatabase.save(new EmployeeDataSt(name));
    }

    @Override
    public EmployeeDataSt getEmployee(UUID id) {
        return employeeDatabase.findById(id).orElseThrow();
    }

    @Override
    public EmployeeDataSt setEmployee(UUID id, String name) {
        EmployeeDataSt employee = employeeDatabase.findById(id).orElseThrow();
        employee.setName(name);
        return employeeDatabase.save(employee);
    }

    @Override
    public boolean removeEmployee(UUID id) {

        for (AssignmentDataSt s : assignmentDatabase.findAll()) {

            if (s.getEmployee_id().equals(id)) {
                throw new IllegalCallerException();
            }
        }
        getEmployee(id);
        employeeDatabase.deleteById(id);
        return true;
    }

}
