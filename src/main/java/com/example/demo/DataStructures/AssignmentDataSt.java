package com.example.demo.DataStructures;

import lombok.Data;
import lombok.NoArgsConstructor;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import java.util.UUID;
import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
@EnableAutoConfiguration
public class AssignmentDataSt {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    private UUID employee_id;

    private UUID reservation_id;

    private String role;

    public AssignmentDataSt(UUID employee_Uuid, UUID reservation_Uuid, String role) {
        this.employee_id = employee_Uuid;
        this.reservation_id = reservation_Uuid;
        this.role = role;
    }

}
