package com.example.demo.DataStructures;

import lombok.Data;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import javax.persistence.*;

@Data
@Entity
@EnableAutoConfiguration
public class StatusDataSt {

    private String Author = "Johannes Walther";
    private String Version;

    // Constructor
    public StatusDataSt(String version) {
        this.Version = version;
    }

}
