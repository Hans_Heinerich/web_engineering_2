package com.example.demo.DataStructures;

import lombok.Data;
import lombok.NoArgsConstructor;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import java.util.UUID;
import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
@EnableAutoConfiguration
public class EmployeeDataSt {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    private String name;

    // Constructor
    public EmployeeDataSt(String name) {
        this.name = name;
    }

}