
package com.example.demo.DataSave;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.stereotype.Repository;

import com.example.demo.DataStructures.AssignmentDataSt;

//@Repository
public interface AssignmentDataStorage extends JpaRepository<AssignmentDataSt, UUID> {

}
