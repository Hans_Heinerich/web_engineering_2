package com.example.demo.DataSave;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.stereotype.Repository;

import com.example.demo.DataStructures.EmployeeDataSt;

//@Repository
public interface EmployeeDataStorage extends JpaRepository<EmployeeDataSt, UUID> {

}
