package com.example.demo.Controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.DataStructures.StatusDataSt;

@RestController
@RequestMapping("/personel/Status")
public class StatusController {

    String versionNum = "1.0.5";

    @GetMapping()
    public ResponseEntity<StatusDataSt> defaultCall() {
        return new ResponseEntity<>(new StatusDataSt(versionNum), HttpStatus.OK);
    }

}
