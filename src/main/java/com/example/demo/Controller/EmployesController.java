package com.example.demo.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.DataBodies.EmployeeAddBody;
import com.example.demo.DataBodies.EmployeeUpdateBody;
import com.example.demo.DataStructures.EmployeeDataSt;
import com.example.demo.callFunctionsInterfaces.EmployeeFunctionsInterface;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

@RestController
@RequestMapping("/personel/employees")
public class EmployesController {

    @Autowired
    EmployeeFunctionsInterface empFunktions;

    @GetMapping()
    public ResponseEntity<List<EmployeeDataSt>> defaultCall() {
        try {
            // getting all Employees
            return new ResponseEntity<>(empFunktions.getAllEmployees(), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            // no Employyes found
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            // unexpected error
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping()
    public ResponseEntity<EmployeeDataSt> postRequest(@RequestBody EmployeeAddBody addRequest) {
        try {
            return new ResponseEntity<EmployeeDataSt>(empFunktions.addEmployee(addRequest.getName()),
                    HttpStatus.CREATED);
        } catch (Exception e) {
            // unexpected error
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/{Id}")
    public ResponseEntity<EmployeeDataSt> getRequest(@PathVariable UUID Id) {
        try {
            // getting Employee
            return new ResponseEntity<>(empFunktions.getEmployee(Id), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            // no fitting Employee found
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            // unexpected error
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{Id}")
    public ResponseEntity<EmployeeDataSt> modifyRequest(@PathVariable UUID Id,
            @RequestBody EmployeeUpdateBody updateRequest) {
        UUID bodyId = updateRequest.getId();

        if (Id != bodyId) {
            return new ResponseEntity<EmployeeDataSt>(HttpStatus.UNPROCESSABLE_ENTITY);
        }
        try {
            // Updating Employee
            return new ResponseEntity<EmployeeDataSt>(
                    empFunktions.setEmployee(updateRequest.getId(), updateRequest.getName()), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            // no fitting Employee found
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            // unexpected error
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        // return null;
    }

    @DeleteMapping("/{Id}")
    public ResponseEntity<Boolean> deleteRequest(@PathVariable UUID Id) {
        try {
            // Deleeting Employee
            return new ResponseEntity<>(empFunktions.removeEmployee(Id), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            // no fitting Employee found
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (IllegalCallerException e) {
            return new ResponseEntity<Boolean>(HttpStatus.UNPROCESSABLE_ENTITY);
        } catch (Exception e) {
            // unexpected error
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}