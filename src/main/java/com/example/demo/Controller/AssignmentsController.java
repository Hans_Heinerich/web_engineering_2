package com.example.demo.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.DataBodies.AssignmentAddBody;
import com.example.demo.DataBodies.AssignmentUpdateBody;
import com.example.demo.DataStructures.AssignmentDataSt;
import com.example.demo.callFunctionsInterfaces.AssignmentFunctionsInterface;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

@RestController
@RequestMapping("/personel/assignments")
public class AssignmentsController {

    @Autowired
    AssignmentFunctionsInterface assignFunktions;

    @GetMapping()
    public ResponseEntity<List<AssignmentDataSt>> defaultCall() {
        try {
            // getting all Assignments
            return new ResponseEntity<>(assignFunktions.getAllAssignments(), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            // no Assignments found
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            // unexpected error
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping()
    public ResponseEntity<AssignmentDataSt> postRequest(@RequestBody AssignmentAddBody addRequest) {
        try {
            return new ResponseEntity<AssignmentDataSt>(
                    assignFunktions.addAssignment(addRequest.getEmployee_id(), addRequest.getReservation_id(),
                            addRequest.getRole()),
                    HttpStatus.CREATED);
        } catch (Exception e) {
            // unexpected error
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{Id}")
    public ResponseEntity<AssignmentDataSt> getRequest(@PathVariable UUID Id) {
        try {
            // getting Assignment
            return new ResponseEntity<>(assignFunktions.getAssignment(Id), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            // no fitting Assignment found
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            // unexpected error
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{Id}")
    public ResponseEntity<AssignmentDataSt> modifyRequest(@PathVariable UUID Id,
            @RequestBody AssignmentUpdateBody updateRequest) {
        UUID bodyId = updateRequest.getId();

        if (Id != bodyId) {
            return new ResponseEntity<AssignmentDataSt>(HttpStatus.UNPROCESSABLE_ENTITY);
        }
        try {
            // Updating Assignment
            return new ResponseEntity<AssignmentDataSt>(
                    assignFunktions.setAssignment(updateRequest.getId(), updateRequest.getEmployee_id(),
                            updateRequest.getReservation_id(), updateRequest.getRole()),
                    HttpStatus.OK);
        } catch (NoSuchElementException e) {
            // no fitting Assignment found
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            // unexpected error
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{Id}")
    public ResponseEntity<Boolean> deleteRequest(@PathVariable UUID Id) {
        try {
            // Deleeting Assignment
            return new ResponseEntity<>(assignFunktions.removeAssignment(Id), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            // no fitting Assignment found
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (IllegalCallerException e) {
            return new ResponseEntity<Boolean>(HttpStatus.UNPROCESSABLE_ENTITY);
        } catch (Exception e) {
            // unexpected error
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}