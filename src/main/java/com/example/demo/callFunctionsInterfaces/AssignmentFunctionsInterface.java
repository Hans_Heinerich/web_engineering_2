package com.example.demo.callFunctionsInterfaces;

import java.util.List;
import java.util.UUID;

import com.example.demo.DataStructures.AssignmentDataSt;

public interface AssignmentFunctionsInterface {

    List<AssignmentDataSt> getAllAssignments();

    AssignmentDataSt addAssignment(UUID employeeUuid, UUID reservationUuid, String role);

    AssignmentDataSt getAssignment(UUID id);

    AssignmentDataSt setAssignment(UUID id, UUID employeeUuid, UUID reservationUuid, String role);

    boolean removeAssignment(UUID id) throws Exception;

}
