package com.example.demo.callFunctionsInterfaces;

import com.example.demo.DataStructures.EmployeeDataSt;

import java.util.List;
import java.util.UUID;

public interface EmployeeFunctionsInterface {

    List<EmployeeDataSt> getAllEmployees();

    EmployeeDataSt addEmployee(String name);

    EmployeeDataSt getEmployee(UUID id);

    EmployeeDataSt setEmployee(UUID id, String name);

    boolean removeEmployee(UUID id) throws Exception;

}
