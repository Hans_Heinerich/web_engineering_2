package com.example.demo.DataBodies;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class EmployeeUpdateBody {
    UUID id;
    String name;
}
