package com.example.demo.DataBodies;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class AssignmentUpdateBody {
    UUID id;
    UUID employee_id;
    UUID reservation_id;
    String role;
}
