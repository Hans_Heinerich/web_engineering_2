package com.example.demo.Authentification;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;

import org.springframework.web.servlet.HandlerInterceptor;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class AssignmentAuthent implements HandlerInterceptor {

    HttpURLConnection conn;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        String requestUrl = request.getRequestURL().toString();
        if (Objects.equals(request.getMethod(), "DELETE") && requestUrl.contains("/personel/assignments")) {
            String[] url = requestUrl.split("/");
            if (checkForAssignment(url[5])) {
                response.setStatus(401);
                return false;
            }
        }
        return true;
    }

    private String getAssignment() {

        BufferedReader reader;
        String line;
        StringBuilder responseContent = new StringBuilder();
        String response = "";
        try {
            URL url = new URL("http://backend-personnel/api/assignments/");
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setConnectTimeout(3000);
            conn.setReadTimeout(3000);

            int status = conn.getResponseCode();

            if (status >= 300) {
                reader = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
            } else {
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            }
            while ((line = reader.readLine()) != null) {
                responseContent.append(line);
            }
            reader.close();
            response = responseContent.toString();
        } catch (MalformedURLException e) {
            System.out.println("MalformedURLException");
        } catch (IOException e) {
            System.out.println("IOException");
        } finally {
            conn.disconnect();
        }
        return response;
    }

    public boolean checkForAssignment(String uuid) {
        return getAssignment().contains(uuid);
    }

}
