package com.example.demo.Authentification;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class Interceptors implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new Tracer());
        registry.addInterceptor(new Authentification());
        registry.addInterceptor(new AssignmentAuthent());
        registry.addInterceptor(new EmployeeAuthent());
    }

}
